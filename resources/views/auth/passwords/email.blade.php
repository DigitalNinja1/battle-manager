@extends('layouts.auth')

@section('content')


    <div class="container">
        <div class="columns">
            <div class="column is-4 is-offset-4">

                <form method="POST" action="{{ route('password.email') }}">
                    @csrf
                    <div class="card">
                        <header class="card-header">
                            <p class="card-header-title">{{ __('Reset Password') }}</p>
                        </header>
                        <div class="card-content">
                            <div class="content">
                                @if (session('status'))
                                        {{ session('status') }}
                                @endif
                                <div class="field">
                                    <label class="label">{{ __('Email') }}</label>
                                    <div class="control">
                                        <input type="text" class="input {{ $errors->has('email') ? ' is-danger' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                                    </div>
                                    @if ($errors->has('email'))
                                    <p class="help is-danger">
                                        {{ $errors->first('email') }}
                                    </p>
                                    @endif
                                </div>
                                <div class="field">
                                    <div class="control">
                                        <button class="button is-primary">{{ __('Send Password Reset Link') }}</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <footer class="card-footer">
                            <p class="card-footer-item">
                                <a href="{{ route('login') }}">{{ __('Login') }}</a>
                            </p>
                            <p class="card-footer-item">
                                <a href="{{ route('register') }}">{{ __('Register') }}</a>
                            </p>
                        </footer>
                    </div>


                </form>


            </div>
        </div>
    </div>

@endsection
