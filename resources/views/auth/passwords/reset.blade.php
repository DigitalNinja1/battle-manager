@extends('layouts.auth')

@section('content')

    <div class="container">
        <div class="columns">
            <div class="column is-4 is-offset-4">
                <form method="POST" action="{{ route('password.request') }}">
                    @csrf
                    <input type="hidden" name="token" value="{{ $token }}">
                    <div class="card">
                        <header class="card-header">
                            <p class="card-header-title">{{ __('Reset Password') }}</p>
                        </header>
                        <div class="card-content">
                            <div class="content">
                                <div class="field">
                                    <label class="label">{{ __('Email') }}</label>
                                    <div class="control">
                                        <input type="text" class="input{{ $errors->has('email') ? ' is-danger' : '' }}" name="email" value="{{ $email or old('email') }}" required autofocus>
                                    </div>
                                    @if ($errors->has('email'))
                                    <p class="help is-danger">
                                        {{ $errors->first('email') }}
                                    </p>
                                    @endif
                                </div>
                                <div class="field">
                                    <label class="label">{{ __('Password') }}</label>
                                    <div class="control">
                                        <input type="password" class="input{{ $errors->has('password') ? ' is-danger' : '' }}" name="password" required>
                                    </div>
                                    @if ($errors->has('password'))
                                    <p class="help is-danger">
                                        {{ $errors->first('password') }}
                                    </p>
                                    @endif
                                </div>
                                <div class="field">
                                    <label class="label">{{ __('Confirm Password') }}</label>
                                    <div class="control">
                                        <input type="password" class="input{{ $errors->has('password_confirmation') ? ' is-danger' : '' }}" name="password_confirmation" required>
                                    </div>
                                    @if ($errors->has('password_confirmation'))
                                    <p class="help is-danger">
                                        {{ $errors->first('password_confirmation') }}
                                    </p>
                                    @endif
                                </div>
                                <div class="field">
                                    <div class="control">
                                        <button class="button is-primary">{{ __('Reset Password') }}</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <footer class="card-footer">
                            <p class="card-footer-item">
                                <a href="{{ route('login') }}">{{ __('Login') }}</a>
                            </p>
                            <p class="card-footer-item">
                                <a href="{{ route('register') }}">{{ __('Register') }}</a>
                            </p>
                        </footer>
                    </div>



                </form>
            </div>
        </div>
    </div>
@endsection
