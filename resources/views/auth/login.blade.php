@extends('layouts.auth')

@section('content')


    <div class="container">
        <div class="columns">
            <div class="column is-4 is-offset-4">
                <form method="POST" action="{{ route('login') }}">
                    @csrf
                    <div class="card">
                        <header class="card-header">
                            <p class="card-header-title">{{ __('Login') }}</p>
                        </header>
                        <div class="card-content">
                            <div class="content">
                                <div class="field">
                                    <label class="label">{{ __('Email') }}</label>
                                    <div class="control">
                                        <input type="text" class="input {{ $errors->has('email') ? ' is-danger' : '' }}" value="{{ old('email') }}" name="email" autofocus required>
                                    </div>
                                    @if ($errors->has('email'))
                                    <p class="help is-danger">
                                        {{ $errors->first('email') }}
                                    </p>
                                    @endif
                                </div>
                                <div class="field">
                                    <label class="label">{{ __('Password') }}</label>
                                    <div class="control">
                                        <input type="password" class="input {{ $errors->has('password') ? ' is-danger' : '' }}" value="{{ old('password') }}" name="password" autofocus required>
                                    </div>
                                    @if ($errors->has('password'))
                                    <p class="help is-danger">
                                        {{ $errors->first('password') }}
                                    </p>
                                    @endif
                                </div>
                                <div class="field">
                                    <label class="checkbox">
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                        {{ __('Remember me')}}
                                    </label>
                                </div>
                                <div class="field">
                                    <div class="control">
                                        <button class="button is-primary">{{ __('Login') }}</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <footer class="card-footer">
                            <p class="card-footer-item">
                                <a href="{{ route('register') }}">{{ __('Register') }}</a>
                            </p>
                            <p class="card-footer-item">
                                <a href="{{ route('password.request') }}">{{ __('Forgot your password?') }}</a>
                            </p>
                        </footer>
                    </div>

                </form>
            </div>
        </div>
    </div>

@endsection
