@extends('layouts.auth')

@section('content')

    <div class="container">

        <div class="columns">
            <div class="column is-4 is-offset-4">
                <form method="POST" action="{{ route('register') }}">
                    @csrf
                    <div class="card">
                        <header class="card-header">
                            <p class="card-header-title">{{ __('Register') }}</p>
                        </header>
                        <div class="card-content">
                            <div class="content">
                                <div class="field">
                                    <label class="label">{{ __('Name') }}</label>
                                    <div class="control">
                                        <input type="text" class="input {{ $errors->has('name') ? ' is-danger' : '' }}" value="{{ old('name') }}" name="name" autofocus required>
                                    </div>
                                    @if ($errors->has('name'))
                                    <p class="help is-danger">
                                        {{ $errors->first('name') }}
                                    </p>
                                    @endif
                                </div>
                                <div class="field">
                                    <label class="label">{{ __('Email') }}</label>
                                    <div class="control">
                                        <input type="text" class="input {{ $errors->has('email') ? ' is-danger' : '' }}" value="{{ old('email') }}" name="email" required>
                                    </div>
                                    @if ($errors->has('email'))
                                    <p class="help is-danger">
                                        {{ $errors->first('email') }}
                                    </p>
                                    @endif
                                </div>
                                <div class="field">
                                    <label class="label">{{ __('Password') }}</label>
                                    <div class="control">
                                        <input type="password" class="input {{ $errors->has('password') ? ' is-danger' : '' }}" name="password" required>
                                    </div>
                                    @if ($errors->has('password'))
                                    <p class="help is-danger">
                                        {{ $errors->first('password') }}
                                    </p>
                                    @endif
                                </div>
                                <div class="field">
                                    <label class="label">{{ __('Confirm Password') }}</label>
                                    <div class="control">
                                        <input type="password" class="input {{ $errors->has('password_confirmation') ? ' is-danger' : '' }}" name="password_confirmation" required>
                                    </div>
                                    @if ($errors->has('password_confirmation'))
                                    <p class="help is-danger">
                                        {{ $errors->first('password_confirmation') }}
                                    </p>
                                    @endif
                                </div>
                                <div class="field">
                                    <div class="control">
                                        <button class="button is-primary">{{ __('Register') }}</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <footer class="card-footer">
                            <p class="card-footer-item">
                                <a href="{{ route('login') }}">{{ __('Login') }}</a>
                            </p>
                            <p class="card-footer-item">
                                <a href="{{ route('password.request') }}">{{ __('Forgot your password?') }}</a>
                            </p>
                        </footer>
                    </div>
                </form>

            </div>
        </div>

    </div>

@endsection
