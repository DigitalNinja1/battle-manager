<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnitUnitsModel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('unit_unit_model', function (Blueprint $table) {
            $table->integer('unit_id')->unsigned();
            $table->integer('unit_model_id')->unsigned();

            $table->foreign('unit_id')->references('id')->on('units')->onDelete('cascade');
            $table->foreign('unit_model_id')->references('id')->on('unit_models')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unit_unit_model');
    }
}
