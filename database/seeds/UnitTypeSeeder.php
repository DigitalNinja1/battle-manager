<?php

use App\Models\ArmyType;
use App\Models\UnitType;
use Illuminate\Database\Seeder;

class UnitTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	$wh40k = ArmyType::where('slug', 'warhammer-40000')->first();
    	$whaos = ArmyType::where('slug', 'warhammer-age-of-sigmar')->first();


    	UnitType::create([
    		'army_type_id' => $wh40k->id,
    		'name' => 'HQ'
    	]);

    	UnitType::create([
    		'army_type_id' => $wh40k->id,
    		'name' => 'Elite'
    	]);

    	UnitType::create([
    		'army_type_id' => $wh40k->id,
    		'name' => 'Troop'
    	]);

    	UnitType::create([
    		'army_type_id' => $wh40k->id,
    		'name' => 'Fast Attack'
    	]);

    	UnitType::create([
    		'army_type_id' => $wh40k->id,
    		'name' => 'Heavy Support'
    	]);

    	UnitType::create([
    		'army_type_id' => $wh40k->id,
    		'name' => 'Dedicated Transport'
    	]);


    	UnitType::create([
    		'army_type_id' => $whaos->id,
    		'name' => 'Leader'
    	]);

    	UnitType::create([
    		'army_type_id' => $whaos->id,
    		'name' => 'Battleline'
    	]);

    	UnitType::create([
    		'army_type_id' => $whaos->id,
    		'name' => 'Leader, Behemoth'
    	]);

    	UnitType::create([
    		'army_type_id' => $whaos->id,
    		'name' => 'Behemoth'
    	]);

    	UnitType::create([
    		'army_type_id' => $whaos->id,
    		'name' => 'Artillery'
    	]);
    }
}
