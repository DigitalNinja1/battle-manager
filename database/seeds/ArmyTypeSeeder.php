<?php

use Illuminate\Database\Seeder;
use App\Models\ArmyType;

class ArmyTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	ArmyType::create([
    		'name' => 'Warhammer 40,000',
    		'slug' => 'warhammer-40000'
    	]);

    	ArmyType::create([
    		'name' => 'Warhammer Age of Sigmar',
    		'slug' => 'warhammer-age-of-sigmar'
    	]);

    }
}
